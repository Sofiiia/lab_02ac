﻿#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <math.h>

#define A 2147483629
#define C 2147483587
#define M 2147483647
#define K 40000
#define RANGE 150

int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);

    int i, freq[RANGE] = { 0 };
    double prob[RANGE], mean = 0.0, variance = 0.0, std_dev = 0.0;
    unsigned int x = 1;

    // Генерація послідовності та підрахунок частоти кожного значення
    for (i = 0; i < K; i++) {
        x = (A * x + C) % M;
        int value = x % RANGE;
        freq[value]++;
    }

    // Розрахунок статистичних ймовірностей
    for (i = 0; i < RANGE; i++) {
        prob[i] = (double)freq[i] / K;
        mean += prob[i] * freq[i]; // середнє значення
    }

    // Розрахунок дисперсії та стандартного відхилення
    for (i = 0; i < RANGE; i++) {
        variance += prob[i] * pow(freq[i] - mean, 2);
    }
    std_dev = sqrt(variance);

    // Виведення результатів
    printf("Частотні інтервали\tСтатистичні ймовірності\n");
    for (i = 0; i < RANGE; i++) {
        printf("%d\t\t\t%.6f\n", freq[i], prob[i]);
    }
    printf("\nСереднє(мат.спод): %.6f\n", mean);
    printf("Дисперсія: %.6f\n", variance);
    printf("Стандартне відхилення: %.6f\n", std_dev);

    return 0;
}
